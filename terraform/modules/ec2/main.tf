module "networking" {
  source = "../networking"
}

resource "aws_instance" "ubuntu" {
 
    ami = data.aws_ami.ubuntu.id
    instance_type = "t2.micro"
    associate_public_ip_address = true
    subnet_id = module.networking.subnet_a
    security_groups = ["${module.networking.sg}"]
    ## Coloque o nome da sua key pair criada na aws.
    ## key_name = "key pair name"
    tags = {
        Name = "idwall"
    }
    user_data = <<-EOF
                #!/bin/bash
                sudo apt update --yes 
                sudo apt install docker.io --yes 
                docker run -dit --name apache -p 80:80 httpd
                EOF
}

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}