output "subnet_a" {
  value = aws_subnet.subnet_a.id
}

output "sg" {
  value = aws_security_group.sg.id
}