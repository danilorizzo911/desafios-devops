variable "region" {
    default = "us-east-1"
}

## subnet do docker

variable cidr_block_a { 
    default = "10.0.1.0/24" 
}

## subnet de acesso

variable cidr_block_b { 
    default = "10.0.2.0/24" 
}

## coloquei meu ip publico!

variable myip { 
    default = "172.0.0.1/32" 
}
