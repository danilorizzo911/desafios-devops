output "public_ip" {
  value = module.ec2.public_ip
}

output "subnet_a" {
  value = module.networking.subnet_a
}

output "sg" {
  value = module.networking.sg
}