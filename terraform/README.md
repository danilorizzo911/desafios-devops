# Desafio 01: Infrastructure-as-code - Terraform

## Resolução

1º Primeiramente precisei instalar o terraform, criar os modulos, no qual, dividi em ec2 e networking. 

2º Depois criei os inputs _(variables.tf)_ de rede e da região conforme solicitado no teste.

3º Em seguida criei os outputs. Devemos criar nos modulos e no outputs.tf principal fazer a chamada do modulo para apresentar no final do _terraform apply_.

4º Configurei tanto um _range_ quanto o meu _ip publico_ para acessar via ssh no _security group_ que eu criei.

5º Por segurança **não** inseri nenhuma _chave.pem_ e nem _minhas credenciais_ de acesso da aws. Geralmente as empresas já tem ou criam na console diretamente.

6º Utilizei a função user_data para instalar o docker e criar o container do apache.

Para que consiga fazer o teste vou explicar abaixo!

## Vamos lá!!!

Requisitos:
> Terraform v0.12+

- Efetue a criação de um novo user na console da aws como tipo de acesso _API_ e a permisão existente _AmazonEC2FullAccess_. Logo em seguida vai gerar o AWS_ACCESS_KEY_ID e a AWS_SECRET_ACCESS_KEY que você vai configurar no terraform. (guarde a secret, pois depois não consiguirá recupera-la, caso perca deverá criar outra access_key)

- Efetue a criação de uma Key Pair na console em EC2 > Key Pair. Guarde o arquivo e o nome gerado para ser utilizado no terraform.

- Abra o arquivo no diretório _terraform/variabels.tf_ e insira as credenciais nos respectivos campos.

- Abra o arquivo no diretório _terraform/modules/ec2/main.tf_ e insira o nome da key pair criada, removendo o campo comentado **key_name**. Assim terá acesso ssh a instancia que será criada.

- Abra o arquivo no diretório _terraform/modules/networking/variables.tf_ e substitua com seu ip publico o campo em negrito "default = "**172.0.0.1**/32".

- Caso não esteja, volte na pasta raiz do teste **terraform** e execute os comandos abaixo:

> terraform init
 
> terraform plan

Não gerando nenhum erro... espero! rs
> terraform apply --auto-approve

- Finalizado, copie o <public_ip> gerado no _Outputs_ e em alguns segundos teste acessando o browser do seu computador ou realizando um _curl_, tem que aparecer o html padrão do apache, conforme solicitado no teste.

- Caso tenha feito todas as criações e configurações das etapas informadas o acesso ssh vai estar funcionando.

- Lembre de dar permissão no key pair.
> chmod 400 suakeypair.pem 
- Faça o acesso.
> ssh -i "suakeypair.pem" ubuntu@<public_ip>

Finish! 

#naminhamaquinafunciona

## Motivação

Recursos de infraestrutura em nubvem devem sempre ser criados utilizando gerenciadores de configuração, tais como [Cloudformation](https://aws.amazon.com/cloudformation/), [Terraform](https://www.terraform.io/) ou [Ansible](https://www.ansible.com/), garantindo que todo recurso possa ser versionado e recriado de forma facilitada.

## Objetivo

- **OK** - Criar uma instância **n1-standard-1** (GCP) ou **t2.micro** (AWS) Linux utilizando **Terraform**.
- **OK** - A instância deve ter aberta somente às portas **80** e **443** para todos os endereços
- **OK** - A porta SSH (**22**) deve estar acessível somente para um _range_ IP definido.
- **Inputs:** A execução do projeto deve aceitar dois parâmetros:
  - **OK** - O IP ou _range_ necessário para a liberação da porta SSH
  - **OK** - A região da _cloud_ em que será provisionada a instância
- **OK** - **Outputs:** A execução deve imprimir o IP público da instância


## Extras

- **OK** - Pré-instalar o docker na instância que suba automáticamente a imagem do [Apache](https://hub.docker.com/_/httpd/), tornando a página padrão da ferramenta visualizável ao acessar o IP público da instância
- **OK** - Utilização de módulos do Terraform

## Notas
- Pode se utilizar tanto AWS quanto GCP (Google Cloud), não é preciso executar o teste em ambas, somente uma.
- Todos os recursos devem ser criados utilizando os créditos gratuitos da AWS/GCP.
- Não esquecer de destruir os recursos após criação e testes do desafio para não haver cobranças ou esgotamento dos créditos.