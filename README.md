# Desafios 

Aqui estão os desafios para a primeira fase de testes de candidatos para às vagas de **Site Reliability Engineer / DevOps**.

1. [Terraform/IaC]
2. [Kubernetes]

Não há diferença de testes para diferentes níveis de profissionais, porém ambos os testes serão avaliados com diferentes critérios, dependendo do perfil da vaga.

Vale reforçar, que a solução deve contemplar os testes de **Terraform/IaC** e **Kubernetes**.