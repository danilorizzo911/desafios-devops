# Desafio 02: Kubernetes

## Resolução

1º Primeiramente precisei criar um cluster kubernetes local, para isso utilizei o minikube, estudei como ele funcionava e suas dependencias. 

2º Depois fazer com que o HELM identificasse e utilizasse uma imagem do docker local foi divertido rs! Pois o minikube utiliza um docker próprio e não o da minha máquina. Foi então que descobri um comando que utiliza o ambiente do docker do minikube como padrão que está no passo a passo. =)

3º Realizei diversos testes, com port-foward, nodeport e depois finalmente utilizando o ingress. Para isso foi necessário habilitar um addons no minikube e adicionar no arquivo hosts da minha máquina para resolver o hostname que eu defini no ingress.

4º Entre outros detalhes não tão importantes, criar um script único sempre é um desafio e tanto, pois um minimo detalhe errado não funciona. Fiquei na dúvida se usamos um shellscript (.sh) ou uma ferramenta como o ansible, foi então que me arrisquei a utilizar o Makefile, pensei de forma simples, não digo que é a melhor forma, mas atendeu o que eu queria. Enfim, espero que dê tudo funcione! 

5º Ah criei tudo para atender todos os objetivos, inclusive o health check pelo deployment e a utilização da variavel **$NAME** em um configmap para alterar o texto de exibição da aplicação.

## Vamos lá!!!

Requisitos:
> Ubuntu (testado na versão 18.04 e na 16)

Abra o terminal: 

Execute o comando a seguir caso não tenha o make instalado:
> sudo apt update --yes && sudo apt install make 

Clone o repositório:
> git clone https://gitlab.com/danilorizzo911/desafios-devops.git

Entre na pasta do desafio:
> cd desafios-devops/kubernetes/

Execute o comando abaixo, vai fazer todas as instalações necessárias e subir o cluster.
> make setup

Execute o comando abaixo, vai utilizar o docker do minikube, assim o build da imagem irá ser feito diretamente no ambiente do minikube.
> eval $(minikube docker-env)

Execute o comando abaixo, vai fazer o deploy da aplicação. 
> make deploy

Existem outras opções de make, Caso deseje ver, só acessar o Makefile.

**Pronto! Agora acesse a aplicação pelo browser ou faça um curl em http://souidwall.digital**.

**Detalhes importantes:** 
- Está subindo o minikube pelo virtualbox que é o padrão, ip do cluster: 192.168.99.100.
- Toda configuração somente funcionará no Ubuntu (logo teremos para as demais distros rsrs) e pode ser que quando execute o make peça a sua senha uma vez (seu usuario precisa ser administrador/root)
- O setup todo está demorando um pouco menos de 10 minutos.
- Todo deploy está sendo feito em um namespace que eu criei chamado **idwall**.
- Pensei em criar também a estrutura do Kustomize ao invés do Helm, caso queiram posso construir também. Só pedir! 

Espero que dê tudo certo! Caso não, ajustarei! =)



## Objetivo
Dentro deste repositório existe um subdiretório **app** e um **Dockerfile** que constrói essa imagem, seu objetivo é:

- **OK** - Construir a imagem docker da aplicação
- **OK** - Criar os manifestos de recursos kubernetes para rodar a aplicação (_deployments, services, ingresses, configmap_ e qualquer outro que você considere necessário)
- **OK** - Criar um _script_ para a execução do _deploy_ em uma única execução.
- **OK** - A aplicação deve ter seu _deploy_ realizado com uma única linha de comando em um cluster kubernetes **local**
- **OK** - Todos os _pods_ devem estar rodando
- **OK** - A aplicação deve responder à uma URL específica configurada no _ingress_


## Extras 
- **OK** - Utilizar Helm [HELM](https://helm.sh)
- **OK** - Divisão de recursos por _namespaces_
- **OK** - Utilização de _health check_ na aplicação
- **OK** - Fazer com que a aplicação exiba seu nome ao invés de **"Olá, candidato!"**

## Notas

* Pode se utilizar o [Minikube](https://github.com/kubernetes/minikube) ou [Docker for Mac/Windows](https://docs.docker.com/docker-for-mac/) para execução do desafio e realização de testes.

* A aplicação sobe por _default_ utilizando a porta **3000** e utiliza uma variável de ambiente **$NAME**

* Não é necessário realizar o _upload_ da imagem Docker para um registro público, você pode construir a imagem localmente e utilizá-la diretamente.